<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Products</title>
</head>

<body>
    <h1 style="text-align:center">Code easy</h1>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div style="max-width: 800px" class="modal-dialog" role="document">
            <form action="" class="form" method="POST">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Thêm sản phẩm</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="title">Tên sản phẩm</label>
                            <input type="text" class="form-control" name="title" id="title" aria-describedby="titlehelp" placeholder="Nhập tên sản phẩm">
                            <small id="titleHelp" class="form-text text-muted"></small>
                        </div>
                        <div class="form-group">
                            <label for="price">Giá bán</label>
                            <input type="text" class="form-control" name="price" id="price" aria-describedby="pricehelp" placeholder="Nhập giá đơn">
                            <small id="priceHelp" class="form-text text-muted"></small>
                        </div>
                        <div class="form-group">
                            <label for="quantity">Số lượng</label>
                            <input type="text" class="form-control" name="quantity" id="quantity" placeholder="Nhập số lượng">
                            <small id="quantityHelp" class="form-text text-muted"></small>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button id="submit" type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="container-fluid" style="text-align:center">
        <div style="text-align:left">
            <button class="btn add btn-success" data-toggle="modal" data-target="#exampleModal">Thêm sản phẩm</button>
            <h5 class="message-success" id="h3"></h5>
        </div>
        <table class="table table-dark">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Title</th>
                    <th scope="col">Price</th>
                    <th scope="col">Quantity</th>
                    <th scope="col">Update time</th>
                    <th scope="col">Edit</th>
                    <th scope="col">Del</th>
                </tr>
            </thead>
            <tbody>


            </tbody>
        </table>
    </div>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script>
        // $('document').ready(
        //     $('.form').submit(function(e) {
        //         e.preventDefault();
        //         let form = $(this);
        //         let acction;
        //         let id;
        //         $.ajax({
        //             url: '<?= base_url() ?>' + 'index.php/product/ajax';
        //             type: 'post';
        //         })
        //     })
        // );


        function renderList() {
            $.ajax({
                url: '<?= base_url() ?>' + 'index.php/product/getall',
                success: function(data) {
                    let datajs = JSON.parse(data);
                    let result = datajs.reduce(function(init, current) {
                        return init + `
                        <tr>
                            <th scope="row">${current.id}</th>
                            <td>${current.title}</td>
                            <td>${current.price}</td>
                            <td>${current.quantity}</td>
                            <td>${current['updated_time']}</td>
                            <td><button data-toggle="modal" data-target="#exampleModal" data-id="${current.id}" class="btn edit btn-primary">Sửa</button></td>
                            <td><button data-id="${current.id}" class="btn del btn-danger">Xóa</button></td>
                        </tr>
                        `
                    }, '');
                    $('tbody').html(result);
                }
            })
        }
        renderList();
        $(document).ready(function() {
            $(document).on('click', '[type="submit"]', function(e) {
                e.preventDefault();
                let title = $('#title').val();
                let quantity = $('#quantity').val();
                let price = $('#price').val();
                let action;
                let id;
                if ($('#submit').data('id')) {
                    action = 'update';
                    id = $('#submit').data('id');

                } else {
                    action = 'create';
                    id = 0;
                }
                $.ajax({
                    url: '<?= base_url() ?>' + 'index.php/product/ajax',
                    type: 'post',
                    data: {
                        title,
                        quantity,
                        price,
                        action,
                        id
                    },
                    success: function(data) {
                        if (data) {
                            let data1 = JSON.parse(data);
                            $('#titleHelp').html(data1.title_error);
                            $('#priceHelp').html(data1.price_error);
                            $('#quantityHelp').html(data1.quantity_error);

                        } else {
                            $('.close').click();
                            renderList();

                        }
                    }
                })
            });
            $(document).on('click', '.del', function(e) {
                e.preventDefault();
                let id = this.dataset.id;
                $.ajax({
                    url: '<?= base_url() ?>' + 'index.php/product/ajax',
                    type: 'post',
                    data: {
                        id
                    },
                    success: function(data) {
                        renderList();

                    }
                })
            });

            $(document).on('click', '.edit', function(e) {
                e.preventDefault();
                let id = this.dataset.id
                $('#exampleModalLabel').html('Sửa sản phẩm id ' + id);
                $('#submit').data('id', id);

                $.ajax({
                    url: '<?= base_url() ?>' + 'index.php/product/ajax',
                    type: 'post',
                    data: {
                        id,
                        getRecord: true
                    },
                    success: function(input) {
                        let data = JSON.parse(input);
                        $('#title').val(data.title);
                        $('#price').val(data.price);
                        $('#quantity').val(data.quantity);
                    }
                })
            });

            // remove id update
            $('.add').on('click', function() {
                $('#submit').data('id', 0);
            })
        })
    </script>
</body>

</html>