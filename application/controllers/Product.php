<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Product extends CI_Controller
{
    public function __construct()
    {

        parent::__construct();

        $this->load->database();

        $this->load->model('Product_model');
        $this->load->helper('url');
    }
    public function index()
    {

        $this->load->view('products');
    }

    public function getall()
    {
        header('Access-Control-Allow-Origin: *');


        die(json_encode($this->Product_model->get_records(), JSON_UNESCAPED_UNICODE));
    }

    public function ajax()
    {
        header('Access-Control-Allow-Origin: *');
        $data = $this->input->post();
        if (isset($data['action'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('title', 'Title', 'required|max_length[100]', array('required' => 'Tên sản phẩm không được trống', 'max_length' => 'Không quá 100 kí tự'));
            $this->form_validation->set_rules('price', 'Price', 'required|greater_than[999]|numeric', array('required' => 'Giá sản phẩm không được trống', 'greater_than' => 'Giá phải là số và lớn hơn 1000'));
            $this->form_validation->set_rules('quantity', 'Quantity', 'required|greater_than[-1]|numeric', array('required' => 'Số lượng sản phẩm không được trống', 'greater_than' => 'Số lượng phải là số và không nhỏ hơn 0'));
            if ($this->form_validation->run()) {
                if ($data['action'] === 'create') {
                    $this->Product_model->create($data);
                }
                if ($data['action'] === 'update') {
                    $this->Product_model->update($data);
                }
            } else {
                $array = array(
                    'price_error' => form_error('price', '<span style="color:red;">', '</span>'),
                    'title_error' => form_error('title',  '<span style="color:red;">', '</span>'),
                    'quantity_error' => form_error('quantity', '<span style="color:red;">', '</span>'),
                );
                die(json_encode($array, JSON_UNESCAPED_UNICODE));
            }
        } else {
            if (isset($data['getRecord'])) {
                $result = $this->Product_model->get_record($data);
                echo json_encode($result, JSON_UNESCAPED_UNICODE);
            } else {
                $this->Product_model->del($data);
            }
        }
    }
}
