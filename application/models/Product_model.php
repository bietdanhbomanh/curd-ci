<?php
class Product_model extends CI_Model
{
    public function get_records()
    {
        $query = $this->db->query('select * from tbl_products order by id desc');
        return $query->result();
    }

    public function get_record($data)
    {
        $id = $data['id'];
        $query = $this->db->query('select * from tbl_products where id =' . $id);
        return $query->result()[0];
    }

    public function create($data)
    {
        $data1 = array('title' => $data['title'], 'price' => $data['price'], 'quantity' => $data['quantity']);

        $title = $data['title'];
        $price = $data['price'];
        $quantity = $data['quantity'];
        $this->db->insert('tbl_products', $data1);
        // $this->db->query('insert into tbl_products (title, price, quantity) values("' . $title . '",' . $price . ',' . $quantity . ')');
    }

    public function update($data)
    {
        $title = $data['title'];
        $price = $data['price'];
        $quantity = $data['quantity'];
        $id = $data['id'];
        $this->db->query('update tbl_products set title = "' . $title . '", price = ' . $price . ', quantity = ' . $quantity . ' where id = ' . $id);
    }

    public function del($data)
    {
        $id = $data['id'];
        $this->db->query('delete from tbl_products where id = ' . $id);
    }
}
